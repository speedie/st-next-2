/* speedie's build of suckless.org's simple terminal (st).
 *
 * This is a fork of suckless's st, a minimal in design, just works terminal emulator that by default does nothing more.
 * Note that this terminal has a list of fonts (See 'Font options') that must be changed before installing.
 * 
 * Additionally, if you use OpenBSD or FreeBSD, you must edit 'options.mk'.
 * If you use GNU/Linux, you should be able to recompile st provided all dependencies are installed.
 *
 * Note: To find out what to enter in *font, run 'fc-list | grep <fontname>'.
 *
 * Font options
 */
static char *font[]                    = {  "Cascadia Code PL:size=8:style=Bold:antialias=true:autohint=true",
										   "DejaVu Sans Mono:size=8:antialias=true:autohint=true",
										   "Noto Color Emoji:size=8:antialias=true:autohint=true", 
										   "fontawesome:size=8:antialias=true:autohint=true",
};


/* Window options */
int borderpx                           = 0; /* Size of the border around the text (in pixels) */
int allowaltscreen					   = 1; /* Allow alt screen */
int allowwindowops					   = 1; /* Allow insecure operations such as setting the clipboard text */
int windowdecorations				   = 1; /* Display window decoration (0/1) */

/* Execution options */
static char *shell					   = "/bin/sh"; /* Shell to execute -e requests with. */
char *stty_args						   = "stty raw pass8 nl -echo -iexten -cstopb 38400";

/* Character bounding-box multipliers */
static float cwscale				   = 1.0;
static float chscale				   = 1.0;

/* Text drawing options */
static int brightbold				   = 0; /* Should bright text also be bold? */
static unsigned int cols			   = 80; /* Number of columns */
static unsigned int rows			   = 24; /* Number of rows */
wchar_t *worddelimiters				   = L" ";

/* Mouse options */
static unsigned int doubleclicktimeout = 300;
static unsigned int tripleclicktimeout = 600;

/* Text latency options */
static uint synctimeout                = 200; /* Time before refreshing in milliseconds */
static double minlatency			   = 8; /* Minimum latency */
static double maxlatency			   = 33; /* Maximum latency */

/* Cursor options */
static unsigned int blinktimeout	   = 800; /* Cursor blink timeout in milliseconds */
static unsigned int cursorthickness	   = 2; /* Cursor thickness in pixels */

/* Boxdraw options
 *
 * 1: Render most of the lines/blocks characters without using the font for
 *    perfect alignment between cells (U2500 - U259F except dashes/diagonals).
 *    Bold affects lines thickness if boxdraw_bold is not 0. Italic is ignored.
 * 0: Disable boxdraw (render all U25XX glyphs normally from the font).
 */
int boxdraw							   = 0; /* Enable boxdraw */
int boxdraw_bold					   = 0; /* Draw boxdraw bold */
int boxdraw_braille					   = 0; /* Render braille as adjecent pixels */

/* Undercurl options
 * 
 * Undercurl allows your terminal to draw undercurl instead of an underline.
 * This looks nice to a lot of people.
 *
 * Curly:
 *  _   _   _   _
 * ( ) ( ) ( ) ( )
 *	 (_) (_) (_) (_)
 *
 * To use Curly:
 * Set undercurl to 1
 *
 * Spiky:
 * /\  /\   /\	/\
 *   \/  \/	  \/
 *
 * To use Spiky:
 * Set undercurl to 2
 *
 * Capped:
 *	_     _     _
 * / \   / \   / \
 *    \_/   \_/
 *
 * To use Capped:
 * Set undercurl to 3
 *
 * To use the default (underline), set undercurl to 0.
 */

static char *understyle          = "1"; /* Undercurl style to use */

/* Bell options */
static int bellvolume				   = 0; /* Bell volume value between -100 and 100. 0 will disable it! */

/* Misc terminal options */
char *termname						   = "st-256color"; /* TERM value, underline will break if it is not set to st-* */
unsigned int tabspaces				   = 4; /* Number of spaces per tab. 8 is common, I prefer 4. */

/* Alpha options */
float alpha							   = 0.8; /* Background alpha/opacity between 0 and 1. */
float gradalpha						   = 0.54; /* Background gradient alpha value that will be changed */
float statalpha						   = 0.46; /* Alpha that will be added to gradalpha */

/* Gradient options */
static int gradient					   = 0; /* Enable alpha gradient (0/1) */
static int graddirection			   = 0; /* Direction of the gradient (0: down, 1: up) */

/* Color options
 *
 * These should be in HTML color format.
 * Most terminals will have 8 regular colors followed by 8 (usually) bright colors.
 * 
 * Bright colors have the 'b' prefix and normal colors have the 'n' prefix.
 * You can also set these through .Xresources.
 *
 * Normal colors */
static char ncolor1[]				   = "#000000"; /* Normal color 1 */
static char ncolor2[]				   = "#ff5555"; /* Normal color 2 */
static char ncolor3[]				   = "#50fa7b"; /* Normal color 3 */
static char ncolor4[]				   = "#f1fa8c"; /* Normal color 4 */
static char ncolor5[]				   = "#bd93f9"; /* Normal color 5 */
static char ncolor6[]				   = "#ff79c6"; /* Normal color 6 */
static char ncolor7[]				   = "#8be9fd"; /* Normal color 7 */
static char ncolor8[]				   = "#bbbbbb"; /* Normal color 8 */

/* Bright colors */
static char bcolor1[]				   = "#44475a"; /* Bright color 1 */
static char bcolor2[]				   = "#ff5555"; /* Bright color 2 */
static char bcolor3[]				   = "#50fa7b"; /* Bright color 3 */
static char bcolor4[]				   = "#f1fa8c"; /* Bright color 4 */
static char bcolor5[]				   = "#bd93f9"; /* Bright color 5 */
static char bcolor6[]				   = "#ff79c6"; /* Bright color 6 */
static char bcolor7[]				   = "#8be9fd"; /* Bright color 7 */
static char bcolor8[]				   = "#ffffff"; /* Bright color 8 */

/* Misc colors */
static char bgcolor[]				   = "#111111"; /* Background color */
static char fgcolor[]				   = "#f8f8f2"; /* Foreground (text) color */
static char cscolor[]				   = "#ffffff"; /* Cursor color */
static char rcscolor[]				   = "#555555"; /* Reverse cursor color */

/* Color index */
unsigned int defaultfg			       = 257; /* Foreground color number */
unsigned int defaultbg				   = 256; /* Background color number */
unsigned int defaultcs				   = 258; /* Selection color number */
unsigned int defaultrc				   = 259; /* Reverse selection color number */
unsigned int defaultattr			   = 11; /* Color for invalid fonts */
unsigned int defaultitalic			   = 7; /* Italic color number */
unsigned int defaultunderline		   = 7; /* Underline color number */
unsigned int selectionbg			   = 257; /* Background selection color number */
unsigned int selectionfg			   = 1; /* Foreground selected color number */
unsigned int ignoreselfg			   = 1;

/* Cursor shape/style options
 *
 * 0: Blinking block
 * 1: Blinking block (default)
 * 2: Steady block ("█")
 * 3: Blinking underline
 * 4: Steady underline ("_")
 * 5: Blinking bar
 * 6: Steady bar ("|")
 * 7: Blinking ccursor cursor
 * 8: Steady ccursor cursor
 */
static unsigned int cursorstyle		   = 1; /* Cursor style to use */
static Rune stcursor			       = 0x2603; /* snowman ("☃") */
static unsigned int mouseshape		   = XC_xterm; /* Mouse shape, default is xterm-like */
static unsigned int mousefg			   = 7; /* Foreground cursor color */
static unsigned int mousebg			   = 0; /* Background cursor color */

/* Externalpipe commands */
static char *listurl[]                 = { "/bin/sh", "-c", "st_urllist", NULL };

/* URL click command
 *
 * This command, followed by the URL will be run by the shell.
 */
static char *urlcmd = "xdg-open";
