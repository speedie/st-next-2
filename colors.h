/* color array */
/* Terminal colors (16 first used in escape sequence) */
static char *colorname[] = {
    /* 8 normal colors */
    [0] = ncolor1, /* black   */
    [1] = ncolor2, /* red     */
    [2] = ncolor3, /* green   */
    [3] = ncolor4, /* yellow  */
    [4] = ncolor5, /* blue    */
    [5] = ncolor6, /* magenta */
    [6] = ncolor7, /* cyan    */
    [7] = ncolor8, /* white   */
                                  
    /* 8 bright colors */
    [8]  = bcolor1, /* black   */
    [9]  = bcolor2, /* red     */
    [10] = bcolor3, /* green   */
    [11] = bcolor4, /* yellow  */
    [12] = bcolor5, /* blue    */
    [13] = bcolor6, /* magenta */
    [14] = bcolor7, /* cyan    */
    [15] = bcolor8, /* white   */
                                   
    /* special colors */
    [256] = bgcolor, /* background */
    [257] = fgcolor, /* foreground */

	[258] = cscolor, /* cursor color */
	[259] = rcscolor, /* rev cursor color */
};
