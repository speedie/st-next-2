/* Mouse binds
 *
 * CONTROL = Left Control/Ctrl key
 * SHIFT   = Left Shift key
 * ALT     = Left Alt key
 * ALTR    = Right Alt key
 * SUPER   = Left Super/Windows/Command key
 * SUPERR  = Right Super/Windows/Command key
 * ANY     = Any of the above modifiers
 * NONE    = No modifier at all
 *
 * Note that overloading Button1 will disable the selection.
 */
static MouseShortcut mshortcuts[] = {
	/* mask                 button   function        argument       release */
	{ ANY,                  Button4, kscrollup,      {.i = 1},		0, -1 },
	{ ANY,                  Button5, kscrolldown,    {.i = 1},		0, -1 },
	{ ANY,				    Button3, selpaste,       {.i = 0},      1     },
	{ SHIFT,	            Button4, ttysend,        {.s = "\033[5;2~"    } },
	{ ANY,				    Button4, ttysend,        {.s = "\031"         } },
	{ SHIFT,                Button5, ttysend,        {.s = "\033[6;2~"    } },
	{ ANY,					Button5, ttysend,        {.s = "\005"         } },
};

